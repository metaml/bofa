{-# LANGUAGE OverloadedStrings #-}
module Kdb.Disk (insert, split) where

import Kdb.Connect (konnect)
import qualified Kdb.Util as Kdb
import Control.Monad (forM_, forever)
import Data.List (intercalate)
import Pipes (Pipe, await, lift, yield)
import System.Environment (getEnv)
import qualified Data.ByteString.Char8 as BS

-- assemble data into `Cpu, a kdb table
insert :: Pipe [String] [String] IO ()
insert = do
  h <- lift $ getEnv "HOSTNAME"
  c <- lift $ konnect "localhost" 5001 ""
  forever $ do
    vas <- await -- [val, <attributes>]
    let t:val:as = vas
        qt = "\"N\"" ++ "$ " ++ (show t)
        qsyms = (map (\a -> "`" ++ a) as)
        row = [qt, "`" ++ h] ++ qsyms ++ [val]
        insertq = BS.pack $ intercalate ";" row
        kdb = BS.concat ["`Disk ", "insert(", insertq, ")"]
    lift $ Kdb.insert c kdb
    yield row

-- order of attributes matters
split :: Pipe String [String] IO ()
split = forever $ do
  l <- await
  lift $ print $ words l
  let time:name:ps = words l
  forM_ [0..(length ps - 1)] $ \i -> yield $ [time, ps !! i, attr i, name]

-- remove "cpu" from output of "collectl --subsys C" gives us the following
attr :: Int-> String
attr i = case i of
  0 -> "readkbs"
  1 -> "readsmerged"
  2 -> "reads"
  3 -> "readsize"
  4 -> "writekbs"
  5 -> "writesmerged"
  6 -> "writes"
  7 -> "writesize"
  8 -> "rwsize"
  9 -> "quelen"
  10 -> "wait"
  11 -> "svctime"
  12 -> "util"
