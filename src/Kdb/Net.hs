{-# LANGUAGE OverloadedStrings #-}
module Kdb.Net (insert, split) where

import Kdb.Connect (konnect)
import qualified Kdb.Util as Kdb
import Control.Monad (forM_, forever)
import Data.List (intercalate)
import Pipes (Pipe, await, lift, yield)
import System.Environment (getEnv)
import qualified Data.ByteString.Char8 as BS

-- assemble data into `Cpu, a kdb table
insert :: Pipe [String] [String] IO ()
insert = do
  h <- lift $ getEnv "HOSTNAME"
  c <- lift $ konnect "localhost" 5001 ""
  forever $ do
    vas <- await -- [val, <attributes>]
    let t:val:as = vas
        qt = "\"N\"" ++ "$ " ++ (show t)
        qsyms = (map (\a -> "`" ++ a) as)
        row = [qt, "`" ++ h] ++ qsyms ++ [val]
        insertq = BS.pack $ intercalate ";" row
        kdb = BS.concat ["`Network ", "insert(", insertq, ")"]
    lift $ Kdb.insert c kdb
    yield row

-- order of attributes matters
split :: Pipe String [String] IO ()
split = forever $ do
  l <- await
  let time:num:name:ps = words l
  forM_ [0..(length ps - 1)] $ \i -> yield $ [time, ps !! i, attr i, name]

-- remove "cpu" from output of "collectl --subsys C" gives us the following
attr :: Int-> String
attr i = case i of
  0 -> "num"
  1 -> "name"
  2 -> "kbin"
  3 -> "pktin"
  4 -> "sizein"
  5 -> "multi"
  6 -> "cmpi"
  7 -> "errsi"
  8 -> "kbout"
  9 -> "pkout"
  10 -> "sizeo"
  11 -> "cmp0"
  12 -> "errs0"
