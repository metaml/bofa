{-# LANGUAGE OverloadedStrings #-}
module Kdb.Cpu (insert, split, mkTable) where

import qualified Data.ByteString.Char8 as BS
import qualified Kdb.Util as Kdb
import Control.Monad (forever)
import Data.List (intercalate)
import Kdb.Connect (konnect)
import Pipes (Pipe, await, lift, yield)
import System.Environment (getEnv)

-- assemble data into `Cpu, a kdb table
insert :: Pipe [String] [String] IO ()
insert = do
  host <- lift $ getEnv "HOSTNAME"
  kdb <- lift $ konnect "localhost" 5001 ""
  forever $ do
    vals <- await
    let time:vs = vals
        qtime = "\"N\"$" ++ (show time)
        row = [qtime, "`" ++ host] ++ vs
        insertq = intercalate ";" row
        q = BS.concat ["`Cpu ", "insert(", BS.pack insertq, ")"]
    lift $ Kdb.insert kdb q
    yield row

-- order of attributes matters
split :: Pipe String [String] IO ()
split = forever $ do
  l <- await
  yield $ words l

mkTable :: IO ()
mkTable = do
  kdb <- konnect "localhost" 5001 ""
  Kdb.insert kdb "Cpu:flip `time`sym`component`user`nice`sys`wait`irq`soft`steal`guest`niceg`idle!()"
