{-# LANGUAGE OverloadedStrings #-}
module Kdb.Mem (insert, split) where

import Kdb.Connect (konnect)
import qualified Kdb.Util as Kdb
import Control.Monad (forM_, forever)
import Data.List (intercalate)
import Pipes (Pipe, await, lift, yield)
import System.Environment (getEnv)
import qualified Data.ByteString.Char8 as BS

-- assemble data into `Cpu, a kdb table
insert :: Pipe [String] [String] IO ()
insert = do
  h <- lift $ getEnv "HOSTNAME"
  c <- lift $ konnect "localhost" 5001 ""
  forever $ do
    vas <- await -- [val, <attributes>]
    let t:val:as = vas
        qt = "\"N\"" ++ "$ " ++ (show t)
        qsyms = (map (\a -> "`" ++ a) as)
        row = [qt, "`" ++ h] ++ qsyms ++ [val]
        insertq = BS.pack $ intercalate ";" row
        kdb = BS.concat ["`Memory ", "insert(", insertq, ")"]
    lift $ print kdb
    lift $ Kdb.insert c kdb
    yield row

-- order of attributes matters
split :: Pipe String [String] IO ()
split = forever $ do
  l <- await
  let time:node:total:ps = words l
  forM_ [0..(length ps - 1)] $ \i -> yield $ [time, ps !! i, attr i, "cpu" ++ node]

attr :: Int-> String
attr i = case i of
  0 -> "used"
  1 -> "free"
  2 -> "slab"
  3 -> "map"
  4 -> "anon"
  5 -> "lock"
  6 -> "inact"
  7 -> "hitpct"

-- #          Node    Total     Used     Free     Slab   Mapped     Anon   Locked    Inact HitPct
-- 04:28:31      0   12012M    2747M    9265M  230152K  186232K  608256K      12K  473848K 100.00
