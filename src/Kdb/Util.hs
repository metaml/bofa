{-# LANGUAGE OverloadedStrings #-}
module Kdb.Util (drop'
                ,insert
                ,join
                ,stdoutLn
                ,trim)
       where

import Kdb.Connect
import Control.Exception (try, throwIO)
import Control.Monad (forever, unless)
import Data.List (intercalate)
import Data.Strings (sTrim, strStartsWith)
import Pipes (Consumer, Pipe, await, lift, yield)
import qualified Data.ByteString.Char8 as BS
import qualified GHC.IO.Exception as G

drop' :: String -> Bool
drop' l = not (strStartsWith l "#" || strStartsWith l "waiting ")

trim :: Pipe String String IO ()
trim = forever $ do
  l <- await
  let l' = sTrim l
  yield l'

join :: Pipe [String] String IO ()
join = forever $ do
  ws <- await
  yield $ intercalate " " ws

stdoutLn :: Consumer String IO ()
stdoutLn = do
  s <- await
  r <- lift $ try $ putStrLn s
  case r of
    -- gracefully terminate on a broken pipe error
    Left e@(G.IOError {G.ioe_type = t})
      -> lift $ unless (t == G.ResourceVanished) $ throwIO e
    Right () -> stdoutLn

insert :: Konnection -> BS.ByteString -> IO ()
insert c q = query c q
