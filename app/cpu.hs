module Main where

import qualified Kdb.Cpu as Cpu
import qualified Data.ByteString as BS
import qualified Pipes.Prelude as P
import Kdb.Util (drop', insert, join, stdoutLn)
import Pipes (Consumer(..), (>~), (>->), await, lift, runEffect)
import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  Cpu.mkTable
  runEffect $ lift getLine >~ P.filter drop' >-> P.filter (not . null) >-> Cpu.split >-> Cpu.insert >-> join >-> stdoutLn
