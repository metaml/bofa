module Main where

import Kdb.Util (drop', insert, join, stdoutLn, trim)
import qualified Kdb.Disk as Disk
import Pipes ((>~), (>->), lift, runEffect)
import qualified Data.ByteString as BS
import qualified Pipes.Prelude as P

main :: IO ()
main = runEffect $ lift getLine >~ P.filter drop' >-> P.filter (not . null) >-> Disk.split >-> Disk.insert >-> join >-> stdoutLn
