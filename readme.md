### collectl to kdb data generator

#### build
  1. cd **bofa**
  2. make build

#### run
  1. in one terminal: run "make q"
  2. in the kdb/q repl: type "\l cpu.q"
  3. in another terminal: run "make cpu-a"
  4. in kdb/q repl: type "CpuMetric"

NB: take a look at the Makefile--it's self explanatory
