\d .threshold

testCpu:{.qunit.assertTrue[(count `.[`Cpu]) > 0;"count Cpu>0"]};
testLabelBlock0b:{.qunit.assertEquals[`.[`labelBlock][0b]];1;"labelBlock 0b==1"};
testLabelBlock1b:{.qunit.assertEquals[`.[`labelBlock][1b]];0;"labelBlock 1b==0"};
testLabelBlock00b:{.qunit.assertEquals[`.[`labelBlock][00b]];1 1;"labelBlock 00b==1 1"};
testLabelBlock11b:{.qunit.assertEquals[`.[`labelBlock][11b]];0 0;"labelBlock 00b==0 0"};
