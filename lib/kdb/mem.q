Memory:([]time:();sym:();attribute:();component:();val:());

MemoryMetric:([time:();sym:();component:()];used:();free:();slab:();map:();anon:();lock:();inact:());
MemoryHostMetric:([time:();sym:()];used:();free:();slab:();map:();anon:();lock:();inact:());

insMemSubRow:{[x];
              t:first x`time;
              s:first x`sym;
              c:first x`component;
              v:first x`val;
              a:first x`attribute;
              $[a=`used;update user:v  from `MemoryMetric where time=t,sym=s,component=c;
                a=`free;update nice:v  from `MemoryMetric where time=t,sym=s,component=c;
                a=`slab;update sys:v   from `MemoryMetric where time=t,sym=s,component=c;
                a=`map;update  wait:v  from `MemoryMetric where time=t,sym=s,component=c;
                a=`anon;update irq:v   from `MemoryMetric where time=t,sym=s,component=c;];
              $[a=`lockupdate  user:v  from `MemoryMetric where time=t,sym=s,component=c;
                a=`anon;update irq:v   from `MemoryMetric where time=t,sym=s,component=c;];
              };

updateMemoryMetric:{[x];
                    t:first x`time;
                    s:first x`sym;
                    c:first x`component;
                    tab:select from MemoryMetric where time=t,sym=s,component=c;
                    r:count tab;
                    $[r=0;`MemoryMetric insert (t;s;c;0;0;0;0;0;0;0);];
                    insMemSubRow x;};

mem:{[x];r:-1#value x;updateMemoryMetric r;};

dispatch:{[x];$[x=`Memory;mem x;];};

.z.vs:{(x;y;dispatch x)};
