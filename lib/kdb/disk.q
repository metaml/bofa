Disk:([]time:();sym:();attribute:();component:();val:());
DiskMetric:([time:();sym:();component:()];reads:();readkbs:();writes:();writekbs:();quelen:();wait:();svctime:();util:());

insDiskSubRow:{[x];
               t:first x`time;
               s:first x`sym;
               c:first x`component;
               v:first x`val;
               a:first x`attribute;
               $[a=`reads;update     reads:v     from `DiskMetric where time=t,sym=s,component=c;
                 a=`readkbs;update   readkbs:v   from `DiskMetric where time=t,sym=s,component=c;
                 a=`writes;update    writes:v    from `DiskMetric where time=t,sym=s,component=c;
                 a=`writekbs;update  writekbs:v  from `DiskMetric where time=t,sym=s,component=c;];
               $[a=`quelen;update    quelen:v    from `DiskMetric where time=t,sym=s,component=c;
                 a=`wait;update      wait:v      from `DiskMetric where time=t,sym=s,component=c;
                 a=`svctime;update   svctime:v   from `DiskMetric where time=t,sym=s,component=c;
                 a=`util;update      util:v      from `DiskMetric where time=t,sym=s,component=c;];};

updateDiskMetric:{[x];
                  t:first x`time;
                  s:first x`sym;
                  c:first x`component;
                  tab:select from DiskMetric where time=t,sym=s,component=c;
                  r:count tab;
                  $[r=0;`DiskMetric insert (t;s;c;0;0;0;0;0;0;0;0);];
                  insDiskSubRow x;};

diskdev:{[x];r:-1#value x;updateDiskMetric r;};

dispatch:{[x];$[x=`Disk;diskdev x;];};

.z.vs:{(x;y;dispatch x)};
