Network:([]time:();sym:();attribute:();component:();val:());
NetworkMetric:([time:();sym:();component:()];kbin:();pktin:();kbout:();pkout:());

insNetworkSubRow:{[x];
                  t:first x`time;
                  s:first x`sym;
                  c:first x`component;
                  v:first x`val;
                  a:first x`attribute;
                  $[a=`kbin;update  kbin:v  from `NetworkMetric where time=t,sym=s,component=c;
                    a=`pktin;update pktin:v from `NetworkMetric where time=t,sym=s,component=c;
                    a=`kbout;update kbout:v from `NetworkMetric where time=t,sym=s,component=c;
                    a=`pkout;update pkout:v from `NetworkMetric where time=t,sym=s,component=c;];};

updateNetworkMetric:{[x];
                 t:first x`time;
                 s:first x`sym;
                 c:first x`component;
                 tab:select from NetworkMetric where time=t,sym=s,component=c;
                 r:count tab;
                 $[r=0;`NetworkMetric insert (t;s;c;0;0;0;0);];
                 insNetworkSubRow x;};

netiface:{[x];r:-1#value x;updateNetworkMetric r;};

dispatch:{[x];$[x=`Network;netiface x;];};

.z.vs:{(x;y;dispatch x)};
