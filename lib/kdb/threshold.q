\d .

labelThreshold:{[ks;t;c;v]
  value "select ",ks,",table:`",(string t),",column:`",(string c),",val:",(string v),",hit:",(string c),">=",(string v)," from ",(string t)};

label:{[t]
  value "select ",(string t)," where i within (0;9)"};

currV:0;
prevV:0;
upC:1;
downC:-1;
labs:();
labelBlock:{[bs]
  currV::0;
  prevV::not first bs;
  upC::1;
  downC::-1;
  labs::();
  {$[x>prevV;(upC::upC+1;currV::upC);x<prevV;(downC::downC-1;currV::downC)];prevV::x;labs,:currV} each bs;
  labs}

hitIds:{[tab]
  hs:exec hit from tab;
  labelBlock[hs]};

addHitIds:{[tab]
  ids:hitIds[tab];
  update hitId:ids from tab};

addThresholdHitColumn:{[sel;tab;col;val]
  t:labelThreshold[sel;tab;col;val];
  tids:addHitIds[t];
  hids:exec hitId from tids;
  ntab:value "update ",(string col),"HitId:hids from ",(string tab);
  ntab};

width:{[tab]t:select width:max(time)-min(time) by hitId from tab;
  update width+00:00:01 from t where width=`time$0};

add:{x+y};
