\d .backup

/ write a table to a native-format file
write:{[tab]value "`:",(string tab)," set ",(string tab);};

/ write a table to a CSV file
writeCsv:{[tab]
  date:.z.d;
  ntab:(string tab),"_tmp";
  value ntab,":update time+.z.d from ",(string tab);
  value "save ","`",ntab,".csv";
  mv:"mv -f ",ntab,".csv"," ",(string tab),"-",(string .z.d),".csv";
  show mv;
  system mv;};

mkCsv:{[tabs]writeCsv each tabs;}
