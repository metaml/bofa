GHCVER := $(shell stack ghc -- --version | awk '{print $$NF}')
LTS := $(shell egrep '^resolver:' stack.yaml | awk '{print $$2}')

SHELL := /bin/bash
PATH := bin:${PATH}

INTERVAL ?= 1
PORT ?= 5001
SCRIPT ?= cpu.q

WORKDERS ?= 1
COPIES ?= 1
dev: clean buildc
# stack
build: bin
	stack build
buildc: bin
	stack build --file-watch
buildv: bin
	stack build --verbose
clean:; stack clean
clobber:; rm -rf .stack-work/*
init:; stack setup

bin:; ln -fs .stack-work/install/x86_64-linux/${LTS}/${GHCVER}/bin

q:; cd lib/kdb && make q PORT=${PORT} SCRIPT=${SCRIPT}

qc:; cd lib/kdb && make qc

cpu: cpu-a
cpu-a:; collectl --interval ${INTERVAL} --subsys C -oT | (export HOSTNAME=a.host && stack exec cpu ${WORKERS} ${COPIES})
cpu-b:; collectl --interval ${INTERVAL} --subsys C -oT | (export HOSTNAME=b.host && stack exec cpu ${WORKERS} ${COPIES})
cpu-c:; collectl --interval ${INTERVAL} --subsys C -oT | (export HOSTNAME=c.host && stack exec cpu ${WORKERS} ${COPIES})

network:; collectl --interval ${INTERVAL} --subsys N -oT | (export HOSTNAME=a.host && stack exec network)
disk:; collectl --interval ${INTERVAL} --subsys D -oT | (export HOSTNAME=a.host && stack exec disk)
memory:; collectl --interval ${INTERVAL} --subsys M -oT | (export HOSTNAME=a.host && stack exec memory)

once:; echo "0 1 2 3 5 8 13 21 34 55 89" | stack exec cpu

DATE ?= $(shell date +"%Y-%m-%d")
backup:
	mdkir -p db/${DATE}
	cd ${DATE} && q


help:;	@echo "to run, you need 3 terminals (a, b, c):"
	@echo "  1. term a: $ make q"
	@echo "  2. term b: $ make cpu-a"
	@echo "  3. term c: $ make cpu-b"
	@echo "NB: always call make cpu-a to define kdb+ table"

ld?:; update-alternatives --query ld
ld:
	sudo update-alternatives --install /usr/bin/ld ld /usr/bin/ld.gold 20
	sudo update-alternatives --install /usr/bin/ld ld /usr/bin/ld.bfd 10
	sudo update-alternatives --config ld

.PHONY: dev build buildc buildv clean clobber init bin \
	q cpu-table cpu-a cpu-b cpu-c \
	help ld ld?
